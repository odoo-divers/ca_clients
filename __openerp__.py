{
    'name' : 'CA par client',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 120,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'crm',
    ],
    'data' : [
    ],

    'installable' : True,
    'application' : False,
}
