# -*- coding: utf-8 -*-
##############################################################################
#
#    ca_client module for Odoo/OpenERP
#    Copyright (c) 2016 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields

class account_invoice_report(models.Model):
    _inherit = "account.invoice.report"

    category_id = fields.Many2many('res.partner.category', related='partner_id.category_id', string='Etiquettes Partenaire')
    category_partner_name = fields.Char(string='Etiquettes Partenaire')

    def _select(self):
        return  super(account_invoice_report, self)._select() + """, (SELECT string_agg(UPPER(rpc.name), ', ' ORDER BY rpc.name)
                                                                    FROM Res_Partner_Res_Partner_Category_Rel rprpcr
                                                                    INNER JOIN Res_Partner_Category rpc ON rpc.id = rprpcr.category_id
                                                                    WHERE rprpcr.partner_id = sub.partner_id) AS category_partner_name"""

account_invoice_report()
